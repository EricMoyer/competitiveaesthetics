This repository contains some experiments in creating a beautiful picture recognizer.

The original concept was to have a database of natural images (downloaded from the web and automatically downsampled by the software) and then have a recognizer (ANN probably) that can distinguish between the natural images and random images. Then we evolve a population of images that the recognizer cannot distinguish but that are as far as possible from the training images (both natural and random). Then the recognizer is trained again to distinguish between the population, natural images, and random images. A problem with this is that the training set of natural images never changes, so eventually we will get overtraining. One solution would be to keep downloading more natural images.

The "competitive aesthetics" moniker comes from a variant idea where the recognizer learns to order images in terms of attractiveness and then the recognizer is used to breed attractive images. Then the most attractive image is added to the training set (with all its comparisons) and the process is repeated. The number of comparisons will increase with the square of the number of images. So, it might be better to give each image a ranking. However, that won't capture the non-monotonic nature of human preferences.




