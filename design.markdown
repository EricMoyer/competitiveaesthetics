Requirements
============

Goal
----
Use competitive evolution to generate good training samples for extracting aesthetic recognition from human input


User Experience
---------------
User is asked to compare pictures and asked which one is better. Then the system thinks for a while and then asks for more comparisons. At any time, the system can be saved and restarted. After quitting, there will be a series of images saved in some location and a file giving their comparative attractivenesses.




Quick and dirty design/Prototype
================================

Overview
--------

Use perl script to run waffles tools for the recognizer training. Image display can be handled by feh (I choose feh over eog because feh lets me set the window title). Evolution can be done in the perl script with the waffles tools providing the evaluation.


Overview with more details
--------------------------

Images are all the same size (256x256)

Each image is stored as a text file giving its representation as if it were PPM (but with commas separating the values) and an actual PPM (with the newlines and dimensions and space-separated values). The text file is named <image name>.training_data. The ppm file is named <image name>.ppm. The PPM is used for the comparisons for the human. The text files are appended to the training file in pairs to create the training data. Before each training line, a comment giving the two images being compared in the following line is appended. Then the two images (from comma separated text files) are appended (separated by a comma) and the expected comparison is appended. This is done during human interaction, so the human supplies the expected comparison. To avoid decision fatigue, only a maximum of a small number of comparisons are provided by randomly selecting among the images. The pairs are entered in the training data so that comparisons are guaranteed symmetric.

If there is no training file, a blank training file is created. Then two images are randomly created and added to the list of images.

The evolution takes place by generating a double population size of random individuals. Then all individiual pairs are output to an ARFF file. Then waffles_predict uses a model to generate evaluation of each pair. The resulting ARFF file is read and the comparisons are assigned back to their original image. The number of times an image wins is its rating. The upper half of the ratings is kept with ties broken by generation order. Then a new generation of is created by Gaussian modification of the extant images to produce one children per image. This is added to the survivors from the last generation to make a new population. When the population of survivors does not change between two generations, the evolution is finished and the top individual is chosen as the new image to add to the training set. (I chose to include the old generation in the selection list because I want convergence to be able to happen to make the termination condition possible. It is true that this won't create images that are as optimal, but the clarity of stopping condition is more important for the prototype.)




